#pragma once
#include <list>
#include <vector>
#include <set>

typedef enum pattern
{
	WIN = 0, //4 
	M1S = 1, //M-1 straight //3
	M1O = 2, //M-1 Onesided //3
	M2S = 3, //2
	M2O = 4, //2
	M3S = 5, // 1
	M3D = 6, // 1
	RIP = 7 //not winnable
} pattern;

typedef enum s_direction
{
	HORIZ = 0,
	VERTI = 1,
	DIAG_LEFT = 2,
	DIAG_RIGHT = 3
} direction;


typedef struct s_pattern
{
	std::pair<int, int> b_pattern;
	std::pair<int, int> e_pattern;
	pattern type;
	direction dir;
	int size;
	int potential;
}  t_pattern;

__interface IGameManager
{
	void getMvt(int *, int *, direction);
	void getPatternValue(t_pattern &);
	void addNewPattern(std::pair<int, int>, std::pair<int, int>, direction, int);
	void immediatePlayerSearch(int, int);
	void findPatterns();
	void cleanup();
	bool hasBeenCalculated(int, int);
public:
	void think();
};

class BasicManager //: public IGameManager
{
private:
	int (*board)[20][20];
	std::vector<std::vector<int>> board_value;
	std::set<std::pair<int, int>> check_coord;
	std::set<t_pattern *> patternlist;
private:
	void getMvt(int *, int *, direction);
	void getPatternValue(t_pattern * temp_pattern);
	void addNewPattern(std::pair<int, int> &, std::pair<int, int> &, direction, int size_pattern);
	void immediatePlayerSearch(int, int);
	void findPatterns();
	std::pair<int, int> evaluateBestMove();
	std::pair<int, int> WeakPlay();
	std::pair<int, int> AnythingAround(int, int);
	std::pair<int, int> strongPlay(t_pattern *);
	void cleanup();
public:
	BasicManager(int (*_board)[20][20]);
	~BasicManager();
	std::pair<int, int> think();
	bool hasBeenCalculated(int, int);
};