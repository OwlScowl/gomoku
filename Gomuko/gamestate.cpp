#include "stdafx.h"
#include <math.h>
#include <windows.h>
#include <list>
#include <iostream>//debug

void BasicManager::getMvt(int *xmvt, int *ymvt, direction dir)
{
	switch (dir)
	{
	case (direction::HORIZ):
		*xmvt = 1;
		*ymvt = 0;
		break;
	case (direction::VERTI):
		*xmvt = 0;
		*ymvt = -1;
		break;
	case (direction::DIAG_LEFT):
		*xmvt = -1;
		*ymvt = -1;
		break;
	case (direction::DIAG_RIGHT):
		*xmvt = 1;
		*ymvt = 1;
		break;
	}
}

//find the value of the pattern(look in .h for details) not const because insertion
void BasicManager::getPatternValue(t_pattern * temp_pattern)
{
	int x = 0;
	int y = 0;
	int xmvt = 0;
	int ymvt = 0;
	int free_spaces = 0;
	int dead_end = 2;
	if (temp_pattern->size >= 4)
		temp_pattern->type = WIN;//calculate winning move
	else
	{
		x = temp_pattern->b_pattern.first;
		y = temp_pattern->b_pattern.second;
		getMvt(&xmvt, &ymvt, temp_pattern->dir);
		free_spaces = 0;
		//check for deadends
		if (0 < x && x < 19 && 0 < y && y < 19)
		{
			if ((*board)[x - xmvt][y - ymvt] == 0)
				dead_end--;
		}
		//find the number of empty spaces
		while ((*board)[x][y] == 0 && 0 < x && x < 19 && 0 < y && y < 19)
		{
			x -= xmvt;
			y -= ymvt;
			free_spaces++;
		}
		//do it again on the other side
		x = temp_pattern->e_pattern.first;
		y = temp_pattern->e_pattern.second;
		if (0 < x && x < 19 && 0 < y && y < 19)
		{
			if ((*board)[x - xmvt][y - ymvt] == 0)
				dead_end--;
		}
		while ((*board)[x][y] == 0 && 0 < x && x < 19 && 0 < y && y < 19)
		{
			x += xmvt;
			y += ymvt;
			free_spaces++;
		}
	}
	if (dead_end == 2)
	{
		temp_pattern->type = RIP;
		return;
	}
	//assign pattern value
	switch (temp_pattern->size)
	{
	case (4):
		temp_pattern->type = WIN;
		break;
	case (3):
		temp_pattern->type = (enum::pattern)(1 + dead_end);
		break;
	case (2):
		temp_pattern->type = (enum::pattern)(3 + dead_end);
		break;
	case (1):
		temp_pattern->type = (enum::pattern)(5 + dead_end);
		break;
	default:
		temp_pattern->type = RIP;
		break;
	}
}

void BasicManager::addNewPattern(std::pair<int, int> &begin_pattern, std::pair<int, int> &end_pattern, direction _dir, int size_pattern)
{
	t_pattern *temp_pattern =  new t_pattern;
	temp_pattern->b_pattern = begin_pattern;
	temp_pattern->e_pattern = end_pattern;
	temp_pattern->dir = _dir;
	temp_pattern->size = size_pattern;
	temp_pattern->potential = 1;
	//get type of pattern
	getPatternValue(temp_pattern);
	patternlist.insert(temp_pattern);
}

void BasicManager::immediatePlayerSearch(int xi, int yi)
{
	std::pair<int, int> crd(xi, yi);
	check_coord.insert(crd);
	int size_pattern = 1;
	int xmvt = 0;
	int ymvt = 0;
	//parse in every ways
	for (int i = 0; i < 4; i++)
	{
		getMvt(&xmvt, &ymvt, (direction)i);
		std::pair<int, int> begin_pattern(xi, yi);
		std::pair<int, int> end_pattern(xi, yi);
		xi -= xmvt;
		yi -= ymvt;
		while (0 < xi && xi < 19 && 0 < yi && yi < 19 && ((*board)[xi][yi] == 1))
		{
			xi -= xmvt;
			yi -= ymvt;
			std::pair<int, int> coord(xi, yi);
			check_coord.insert(coord);
			size_pattern++;
		}
		begin_pattern.first = xi + xmvt;
		begin_pattern.second = yi + ymvt;
		xi = end_pattern.first;
		yi = end_pattern.second;
		xi += xmvt;
		yi += ymvt;
		while ((*board)[xi][yi] == 1 && 0 < xi && xi < 19 && 0 < yi && yi < 19)
		{
			xi += xmvt;
			yi += ymvt;
			std::pair<int, int> coord(xi, yi);
			check_coord.insert(coord);
			size_pattern++;
		}
		end_pattern.first = xi - xmvt;
		end_pattern.second = yi - ymvt;
		if (begin_pattern != end_pattern)
			addNewPattern(begin_pattern, end_pattern, direction::HORIZ, size_pattern);
	}
}

bool BasicManager::hasBeenCalculated(int x, int y)
{
	std::pair<int, int> temp(x, y);
	if (check_coord.find(temp) == check_coord.end())
		return (false);
	return (true);
}

void BasicManager::findPatterns()
{
	for (int x = 0; x < 20; x++)
	{
		for (int y = 0; y < 20; y++)
		{
			if ((*board)[x][y] == 1 && hasBeenCalculated(x, y) == false)
			{
				immediatePlayerSearch(x, y);
			}
		}
	}
}

BasicManager::BasicManager(int (*_board)[20][20])
{
	board = _board;
}

BasicManager::~BasicManager()
{
}

void BasicManager::cleanup()
{
	for (std::set<t_pattern *>::iterator it = patternlist.begin(); it != patternlist.end(); ++it)
		delete *it;
	patternlist.clear();
}

//look for any winnable play around
std::pair<int, int> BasicManager::AnythingAround(int x, int y)
{
		for (int xmvt = -1; xmvt <= 1; x++)
		{
			for (int ymvt = -1; ymvt <= 1; y++)
			{
				int x1 = x + xmvt;
				int y1 = y + ymvt;
				if (xmvt != 0 && ymvt !=0 && 0 <= x1 && x1 <= 19 && 0 <= y1 && y1 <= 19 &&
					(*board)[x1][y1] == 0)
				{
					return (std::pair<int, int>(x1, y1));
				}
			}
		}
	return (std::pair<int, int>(-1, -1));
}

//No good moves, try to create a 2, or put a stone close to others
std::pair<int, int> BasicManager::WeakPlay()
{
	std::pair<int, int> temp(-1, -1);
	for (int x = 0; x < 20; x++)
	{
		for (int y = 0; y < 20; y++)
		{
			if ((*board)[x][y] == 1)
			{
				temp = AnythingAround(x, y);
				if (temp.first != -1 && temp.first != -1)
					return (temp);
			}
		}
	}
	return (temp);
}

std::pair<int, int> BasicManager::strongPlay(t_pattern * best)
{
	int xmvt = 0;
	int ymvt = 0;
	getMvt(&xmvt, &ymvt, best->dir);
	int x = best->b_pattern.first - xmvt;
	int y = best->b_pattern.second - ymvt;
	if (0 <= x && x <= 19 && 0 <= y && y <= 19 && (*board)[x][y] == 0)
	{
		return (std::pair<int, int>(x, y));
	}
	x = best->e_pattern.first + xmvt;
	y = best->e_pattern.second + ymvt;
	if (0 <= x && x <= 19 && 0 <= y && y <= 19 && (*board)[x][y] == 0)
	{
		return (std::pair<int, int>(x, y));
	}
	return (std::pair<int, int>(-1, -1));
}

std::pair<int, int> BasicManager::evaluateBestMove()
{
	if (patternlist.size() == 0)
		return WeakPlay();
	t_pattern *best_pattern = *(patternlist.begin());
	if (best_pattern->type == RIP)
		return WeakPlay();
	for (std::set<t_pattern *>::iterator it = patternlist.begin(); it != patternlist.end(); ++it)
	{
		if ((int)(*it)->type < (int)best_pattern)
		{
			best_pattern = *it;
		}
	}
	return strongPlay(best_pattern);
}

std::pair<int,int> BasicManager::think()
{
	std::pair<int, int> best_move;
	findPatterns();
	best_move = evaluateBestMove();
	cleanup();
	return (best_move);
}